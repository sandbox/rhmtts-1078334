By default UberPOS requires users click messages to make them disappear before they can enter another command. For a project we at ICA were doing, this was undesirable. Hence we had to create this module.
 
Unfortunately, the only way to make messages non-blocking was to make rather hackish use of two aspects of the UberPOS code, to wit:

* You can run your own interpreters before the default interpreter is set.
* Commands that return non-true still get executed.

What we did is create a command that always returns non-true but that before doing so sets $_SESSION['uberpos_interpreter'] to the name of our custom interpreter. In our custom interpreter we then check if $_SESSION['uberpos_clear_message'] is set. If it is, and the command issued is not "CL" we clear $_SESSION['uberpos_clear_message'] (and the order as well, if $_SESSION['uberpos_clear_order']);

We are aware that this is an ugly way to solve the problem. A much better solution would be to make an option for UberPOS whether you want messages to be blocking or not and to fix up the default interpreter to make use of that option.
